cubature (1.0.4+ds-2) unstable; urgency=medium

  * Explicitly cast pointer to long double (Closes: #1091244)
  * Push Standards-Version to 4.7.0, no changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 24 Dec 2024 12:43:49 +0100

cubature (1.0.4+ds-1) unstable; urgency=medium

  * Make build cross buildable (Closes: #984877)
    - Thank you, Helmut Grohne
  * New upstream version 1.0.4+ds
  * Declare compliance with policy 4.5.1
  * Bump compat version to 13

 -- Nilesh Patra <nilesh@debian.org>  Sat, 14 Aug 2021 02:44:02 +0530

cubature (1.0.3+ds-4) unstable; urgency=medium

  * Replace DEB_BUILD_MULTIARCH with DEB_HOST_MULTIARCH
  * Include dpkg buildflags to make package cross-buildable
  * Add myself to uploaders
  * Replace deprecated adttmp with AUTOPKGTEST_TMP
  * Add "Rules-Requires-Root: no"
  * Bump watch version to 4

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 20 Feb 2021 15:44:12 +0530

cubature (1.0.3+ds-3) unstable; urgency=low

  * Check for fftw3l in d/rules

 -- Ole Streicher <olebole@debian.org>  Sat, 19 Oct 2019 15:28:40 +0200

cubature (1.0.3+ds-2) unstable; urgency=low

  * Push Standards-Version to 4.4.1. No changes required.
  * Use fftw instead of fftwl on archs that don't provide it
  * Add more build time tests

 -- Ole Streicher <olebole@debian.org>  Sat, 19 Oct 2019 11:54:28 +0200

cubature (1.0.3+ds-1) unstable; urgency=low

  * New package. Closes: #940903

 -- Ole Streicher <olebole@debian.org>  Sat, 21 Sep 2019 18:31:27 +0200
