#!/bin/sh

set -e

cp test.c ${AUTOPKGTEST_TMP}
cd ${AUTOPKGTEST_TMP}

gcc -o htest test.c -lcubature -lm
gcc -o ptest -DPCUBATURE test.c -lcubature -lm

#    ./htest <dim> <tol> <integrand> <maxeval>
#
# where `<dim>` = #dimensions, `<tol>` = relative tolerance, `<integrand>` is
# 0–7 for one of eight possible test integrands (see below) and `<maxeval>`
# is the maximum number of function evaluations (0 for none, the default).
# Similarly for `ptest` (which tests the `pcubature` function).

echo
echo "0: a product of cosine functions"
./htest 3 1e-5 0
./ptest 3 1e-5 0

echo
echo "1: a Gaussian integral of exp(-x²), remapped to [0,∞) limits"
./htest 3 1e-5 1
./ptest 3 1e-5 1

# Test 2 takes too long and is therefore excluded
#echo
#echo "2: volume of a hypersphere (integrating a discontinuous function!)"
#./htest 3 1e-5 2
#./ptest 3 1e-5 2

echo
echo "3: a simple polynomial (product of coordinates)"
./htest 3 1e-5 3
./ptest 3 1e-5 3

echo
echo "4: a Gaussian centered in the middle of the integration volume"
./htest 3 1e-5 4
./ptest 3 1e-5 4

echo
echo "5: a sum of two Gaussians"
./htest 3 1e-5 5
./ptest 3 1e-5 5

echo
echo "6: an example function by Tsuda, a product of terms with near poles"
./htest 3 1e-5 6
./ptest 3 1e-5 6

echo
echo "7: a test integrand by Morokoff and Caflisch, a simple product of"
echo "   dim-th roots of the coordinates (weakly singular at the boundary)"
./htest 3 1e-5 7
./ptest 3 1e-5 7
